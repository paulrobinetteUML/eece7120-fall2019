#!/usr/bin/env python

import rospy
from duckietown_msgs.msg import Twist2DStamped

if __name__ == '__main__':
    rospy.init_node('open_loop', anonymous=True)
    pub = rospy.Publisher('~car_cmd', Twist2DStamped, queue_size=1)
    rate = rospy.Rate(10) # 10hz
    t_start = rospy.get_time()
    while not rospy.is_shutdown():
        t = rospy.get_time()
        msg = Twist2DStamped()
        dt = t - t_start
        if dt > 10 and dt < 15:
            msg.v = 0.75
            msg.omega = 10.0        
        elif dt > 15 and dt < 20:
            msg.v = 0.75
            msg.omega = -10.0        
        else:
            msg.v = 0
            msg.omega = 0
        pub.publish(msg)
        rate.sleep()
